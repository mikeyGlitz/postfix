# Postfix

An extension of [bokysan/postfix](https://github.com/bokysan/docker-postfix)
which includes optional SSL configuration

## Getting Started

This docker container utilizes the [bokysan/postfix](https://github.com/bokysan/docker-postfix)
Docker container as a base. This container extends the bokysan/postfix container
to allow SSL-based Postfix configuration when the certificates are supplied.

Mount the secrets in your Docker container to `/mnt/certs`.
The container expects the following files to be present in order to continue
running and setting up the TLS configuration:

- `tls.crt`
- `tls.key`
- `ca.crt`

## Development

This container doesn't diverge much from its base image.
I added a new script `certinstaller.sh` to push the TLS
configuration into the postfix configuration.

## References

- https://www.ssls.com/knowledgebase/installing-and-configuring-an-ssl-certificate-on-postfix-dovecot-mail-server/

