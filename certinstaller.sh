#!/bin/sh

# This script expects certificates the following
# certificate files:
# ca.crt - The Certificate Authority (CA) certificate chain
# tls.crt - The certificate file
# tls.key - The certificate key file

# Certificate directory
CERTDIR=/etc/ssl

# mounted certificate folder
MOUNT_CERTDIR=/mnt/certs

# Check for the presence of a CERTDIR folder
if [ -d "$MOUNT_CERTDIR" ]; then
    if [ "$(ls -A $MOUNT_CERTDIR)" ]; then
        # Copy all files into the certificate folder
        cp "$MOUNT_CERTDIR/ca.crt" "$CERTDIR/ca.crt"
        cp "$MOUNT_CERTDIR/tls.crt" "$CERTDIR/tls.crt"
        cp "$MOUNT_CERTDIR/tls.key" "$CERTDIR/tls.key"

        CA="$CERTDIR/ca.crt"
        CERT="$CERTDIR/tls.crt"
        KEY="$CERTDIR/tls.key"

        FILES=($CA $CERT $KEY)

        for FILE in $FILES
        do
            # We cannot do a TLS configuration if we're missing a 
            # certificate file
            [[ ! -f $FILE ]] && exit -1
        done
        # write in the TLS configuration
        echo "smtpd_tls_cert_file = $CERT" >> /etc/postfix/main.cf
        echo "smtpd_tls_key_file = $KEY" >> /etc/postfix/main.cf
        echo 'smtpd_use_tls = yes' >> /etc/postfix/main.cf
        echo 'smtpd_tls_session_cache_database = lmdb:${data_directory}/smtpd_scache' >> /etc/postfix/main.cf
        echo 'smtp_tls_session_cache_database = lmdb:${data_directory}/smtp_scache' >> /etc/postfix/main.cf

    fi
fi
