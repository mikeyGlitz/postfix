FROM boky/postfix

LABEL maintainer="Michael Glitzos <michael.glitzos@gmail.com>"

ADD certinstaller.sh /docker-init.db/
